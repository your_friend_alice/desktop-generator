#!/usr/bin/env python3
import cairo
import sys, os, re, random, math, argparse
rng = random.SystemRandom()

def parseSize(size):
    if type(size) in [list, tuple]:
        return size
    else:
        return [int(i) for i in size.split('x')]

def getColors(filename):
    colorMap = {}
    with open(filename) as file:
        for line in file:
            match = re.search('\*?color([0-9]+):\s*#([0-9a-f]+)', line.strip())
            if match:
                color = match[2]
                if len(color) == 3:
                    channels = (color[0], color[1], color[2])
                elif len(color) == 6:
                    channels = (color[0:2], color[2:4], color[4:7])
                colorMap[int(match[1])] = tuple(int(c, 16)/256.0 for c in channels)
    colors = tuple(i[1] for i in sorted(colorMap.items()))
    return tuple(zip(colors[0:8], colors[8:16]))

def makeGrid(size, pitch):
    """
    return a list of rows, each containing a list of coordinates
    """
    overhang = tuple( (pitch-(n % pitch)) % pitch for n in size )
    points = [
            [
                (
                    int(column-overhang[0]/2),
                    int(row-overhang[1]/2)
                    )
                for column in range(0, size[0] + overhang[0]+1, pitch)
                ]
            for row in range(0, size[1] + overhang[1]+1, pitch)
            ]
    return points

def gridSize(grid):
    return (len(grid[0]), len(grid))

def scronch(grid, radius):
    size = gridSize(grid)
    for x in range(0, size[0]):
        for y in range(0, size[1]):
            node = grid[y][x]
            offset = [0,0]
            if x > 0 and x < size[0]-1:
                offset[0] = rng.uniform(-radius, radius)
            if y > 0 and y < size[1]-1:
                offset[1] = rng.uniform(-radius, radius)
            grid[y][x] = tuple(n + o for n, o in zip(node, offset))
    return grid

def paintTile(context, grid, gridPoints, color, strokeColor=None):
    points = tuple(grid[p[1]][p[0]] for p in gridPoints)
    # set path
    context.move_to(*points[0])
    for point in points[1:]:
        context.line_to(*point)
    context.close_path()
    # draw stroke
    context.set_source_rgb(*(color if strokeColor is None else strokeColor))
    context.stroke_preserve()
    # draw fill
    context.set_source_rgb(*color)
    context.fill()
    # draw diagonal if stroke width is 0
    if context.get_line_width() == 0:
        context.set_line_width(0.5)
        context.move_to(*points[1])
        context.line_to(*points[2])
        context.stroke_preserve()
        context.set_line_width(0)

def paintSquare(context, grid, gridPoint, colorPair, flip=False, strokeColor=None):
    orientations = (
        (
            ((0,0), (0,1), (1,0)),
            ((1,1), (0,1), (1,0))
            ),
        (
            ((1,0), (0,0), (1,1)),
            ((0,1), (0,0), (1,1))
            )
    )
    for color, points in zip(colorPair, orientations[int(flip)]):
        paintTile(
                context = context,
                grid = grid,
                gridPoints = tuple(tuple(a + b for a, b in zip(point, gridPoint)) for point in points),
                color = color,
                strokeColor = strokeColor
                )

def fill(context, color):
    context.set_source_rgb(*color)
    context.rectangle(0, 0, context.get_target().get_width(), context.get_target().get_height())
    context.fill()

def draw(colorPairs, size, pitch=100, randomFlips=True, scronchRadius=0, lineColor=None, lineWidth=0, fillBottom=None):
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, size[0], size[1])
    context = cairo.Context(surface)
    context.set_line_join(cairo.LINE_JOIN_ROUND)
    # set line width
    if lineColor:
        context.set_line_width(max(1, lineWidth))
    elif scronchRadius != 0:
        context.set_line_width(1)
    else:
        context.set_line_width(0)
    # fill background only if it'll be exposed at the bottom
    if fillBottom is not None:
        fill(context, fillBottom)

    grid = scronch(makeGrid(size, pitch), scronchRadius)
    for x in range(0, gridSize(grid)[0]-1):
        for y in range(0, gridSize(grid)[1]-(1 if fillBottom is None else 2)):
            paintSquare(
                    context = context,
                    grid = grid,
                    gridPoint = (x, y),
                    colorPair = rng.choice(colorPairs),
                    flip = random.choice((0,1)) if randomFlips else 0,
                    strokeColor = lineColor if lineColor else None
                    )
    return surface

parser = argparse.ArgumentParser(
        description = 'Create a pretty desktop background from your terminal color scheme'
        )
parser.add_argument(
        '-f', '--file',
        type=str,
        dest='file',
        default='~/.Xdefaults',
        help='The X resources file to use for colors (default: %(default)s)'
        )
parser.add_argument(
        '-s', '--size',
        required=True,
        type=str,
        dest='size',
        help='The size of the image to produce, written in the form <width>x<height> (for example: 1920x1080)'
        )
parser.add_argument(
        '--pitch',
        type=int,
        dest='pitch',
        default=100,
        help='The size of the grid squares (default: %(default)i)'
        )
parser.add_argument(
        '--scronch',
        type=float,
        dest='scronch',
        default=0,
        help='How hard to randomize the position of the points on the grid, best between 0 and 0.5 (default: %(default)f)'
        )
parser.add_argument(
        '--no-flips',
        action='store_false',
        dest='flips',
        help='Keep all the squares oriented the same way'
        )
parser.add_argument(
        '--borders',
        action='store_true',
        dest='borders',
        help="Draw borders around each tile, using the terminal's black color"
        )
parser.add_argument(
        '--dark-bottom',
        action='store_true',
        dest='darkBottom',
        help="Color the bottom row with the terminal's black color, for blending with a status bar"
        )
parser.add_argument(
        'outfile',
        type=str,
        help='The filename to write to'
        )
args = parser.parse_args()
colorPairs = getColors(os.path.expanduser(args.file))
darkColor=colorPairs[0][1]
surface = draw(
        colorPairs = colorPairs,
        size = parseSize(args.size),
        pitch = args.pitch,
        randomFlips = args.flips,
        scronchRadius = args.scronch/2 * args.pitch,
        fillBottom = darkColor if args.darkBottom else None,
        lineColor = darkColor if args.borders else None,
        lineWidth = 2
        )
surface.write_to_png(args.outfile)
