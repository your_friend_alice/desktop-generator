# desktop-generator

```
./desktop-generator.py --size 1280x720 --pitch 80 --borders --scronch 0.4 example.png
```
![](example.png)

This image uses the [Solarized pallette](https://ethanschoonover.com/solarized/). The relevant section of my `.Xdefaults` file:

```
*color0: #073642
*color1: #dc322f
*color2: #859900
*color3: #b58900
*color4: #268bd2
*color5: #d33682
*color6: #2aa198
*color7: #eee8d5
*color8: #002b36
*color9: #cb4b16
*color10: #586e75
*color11: #657b83
*color12: #839496
*color13: #6c71c4
*color14: #93a1a1
*color15: #fdf6e3
```

## Requirements

- Python 3
- [pycairo](https://www.cairographics.org/pycairo/)
- An `.Xdefaults` or `.Xresources` file with a terminal color scheme in it

## Usage
```
./desktop-generator.py --help
```
